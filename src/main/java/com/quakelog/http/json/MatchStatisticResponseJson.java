package com.quakelog.http.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class MatchStatisticResponseJson {
    @JsonProperty("total_kills")
    private Integer totalKills;

    @JsonProperty("players")
    private String[] players;

    @JsonProperty("kills")
    private Map<String, Integer> kills;
}
