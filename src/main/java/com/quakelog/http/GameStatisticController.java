package com.quakelog.http;

import com.quakelog.domains.Statistic;
import com.quakelog.http.json.MatchStatisticResponseJson;
import com.quakelog.usecases.GameStatistic;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api/stats")
@Api(tags = "Estastisticas do jogo", produces = MediaType.APPLICATION_JSON_VALUE)
public class GameStatisticController {

    @Autowired
    private GameStatistic gameStatistic;

    @ApiOperation(value = "Recurso para consultar as estastisticas do jogo quake")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Map<String, MatchStatisticResponseJson>>> checkHumanDna() {
        final List<Statistic> statisticList = gameStatistic.checkstats();

        List<Map<String, MatchStatisticResponseJson>> response = buildGameStatisticResponseJson(statisticList);;

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private List<Map<String, MatchStatisticResponseJson>> buildGameStatisticResponseJson(final List<Statistic> statisticList) {
        List<Map<String, MatchStatisticResponseJson>> games = new ArrayList<>();

        for (Statistic s: statisticList) {
            Map<String, MatchStatisticResponseJson> game = new HashMap<>();
            game.put(s.getName(), buildMatchStatisticResponseJson(s.getKills(), s.getTotalKills()));
            games.add(game);
        }

        return games;
    }

    private MatchStatisticResponseJson buildMatchStatisticResponseJson(final Map<String, Integer> kills, Integer totalKills) {
        MatchStatisticResponseJson matchStatisticResponseJson = new MatchStatisticResponseJson();
        Collection<String> values = kills.keySet();
        String[] targetArray = values.toArray(new String[values.size()]);
        matchStatisticResponseJson.setPlayers(targetArray);
        matchStatisticResponseJson.setKills(kills);
        matchStatisticResponseJson.setTotalKills(totalKills);
        return matchStatisticResponseJson;
    }
}
