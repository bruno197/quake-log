package com.quakelog.usecases;

import com.quakelog.domains.Game;
import com.quakelog.gateways.factory.ParserFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ExtractData {
    @Value("${elasticsearch.enable}")
    private Boolean elasticsearch;

    @Autowired
    private ParserFactory parserFactory;

    @Autowired
    private ParserGameData parserGameData;

    public List<Game> readData() {
        return parserGameData.getGame(parserFactory.create(elasticsearch).getLines());
    }

}
