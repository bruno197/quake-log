package com.quakelog.usecases.exception;

public class GameStatisticException extends RuntimeException {
    private static final long serialVersionUID = 5502347862043183093L;

    private static final String MESSAGE = "Erro ao calcular as estastisticas";

    public GameStatisticException() {
        super(MESSAGE);
    }
}
