package com.quakelog.usecases.exception;

public class UnprocessableLineException extends RuntimeException {
    private static final long serialVersionUID = 5502347862043183093L;

    private static final String MESSAGE = "Linha invalida para o processamento";

    public UnprocessableLineException() {
        super(MESSAGE);
    }
}
