package com.quakelog.usecases;

import com.quakelog.domains.Game;
import com.quakelog.domains.Phrase;
import com.quakelog.usecases.exception.UnprocessableLineException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ParserGameData {
    private static final String KILL = "Kill";
    private static final String KILLED = "killed";
    private static final String LINE_EMPTY     = "------------------------------------------------------------";

    public List<Game> getGame(final List<String> lines) {
        List<Game> games = new ArrayList<>();
        Game game = null;
        for (String line: lines) {
            if(LINE_EMPTY.equals(parserLineToGame(line))) {
                if(game != null) {
                    if(!game.getPhraseList().isEmpty()) {
                        games.add(new Game("game_"+(games.size()+1), game.getPhraseList()));
                    }
                }
                game = new Game();
                game.setPhraseList(new ArrayList<>());
            }
            try {
                Phrase phrase = parserLineToPhrase(line);
                game.getPhraseList().add(phrase);
            } catch (Exception e) {
                log.info("Line error: ", e.getMessage());
            }
        }
        return games;
    }

    private String parserLineToGame(String line) {
        String[] splitLine = line.split(":");

        if(splitLine.length >= 2 && splitLine[1].contains(LINE_EMPTY)) {
            return LINE_EMPTY;
        }

        return null;
    }

    private Phrase parserLineToPhrase(String line) {
        String[] splitLine = line.split(":");
        final Phrase phrase = new Phrase();

        if(splitLine.length >= 2 && splitLine[1].contains(KILL)) {
            findPhrase(splitLine[3], phrase);
            return phrase;
        } else {
            throw new UnprocessableLineException();
        }
    }

    private void findPlayer(final String line, final Phrase phrase) {
        if(line.contains("<world>")) {
            phrase.setAction("killed");
            phrase.setWho("<world>");
            phrase.setPlayer(line.substring(getBeginPlayerIndex(line, KILLED.length()), line.indexOf("by")).trim());
        } else {
            phrase.setAction("kill");
            phrase.setWho(line.substring(getBeginPlayerIndex(line, KILLED.length()), line.indexOf("by")).trim());
            phrase.setPlayer(line.substring(0, getBeginPlayerIndex(line, 0)).trim());
        }
    }

    private void findPhrase(String line, Phrase phrase) {
        findPlayer(line, phrase);
        findMOD(line, phrase);
    }

    private void findMOD(final String line, final Phrase phrase) {
        phrase.setCause(line.substring(line.indexOf("MOD")));
    }

    private int getBeginPlayerIndex(final String line, final int plus) {
        return line.indexOf(KILLED)+plus;
    }
}
