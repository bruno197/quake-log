package com.quakelog.usecases;

import com.quakelog.domains.Game;
import com.quakelog.domains.Phrase;
import com.quakelog.domains.Statistic;
import com.quakelog.usecases.exception.GameStatisticException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class GameStatistic {
    public static final String WORLD = "<world>";
    @Autowired
    private ExtractData extractData;

    public List<Statistic> checkstats() {
        try {
            return calculate(extractData.readData());
        } catch (Exception e) {
            throw new GameStatisticException();
        }
    }

    private List<Statistic> calculate(final List<Game> games) {
        List<Statistic> statisticList = new ArrayList<>();

        for (Game game : games) {
            statisticList.add(
                    new Statistic(
                            game.getMatch(),
                            game.getPhraseList().size(),
                            generateMap(game.getPhraseList())
                    )
            );
        }

        return statisticList;
    }

    private Map<String, Integer> generateMap(final List<Phrase> phraseList) {
        Map<String, Integer> kills = new HashMap<>();
        for (Phrase phrase: phraseList) {
            Integer v = kills.getOrDefault(phrase.getPlayer(), 0);

            if(!phrase.getWho().equals(WORLD) && !kills.containsKey(phrase.getWho())) {
                kills.put(phrase.getWho(), 0);
            }
            kills.put(phrase.getPlayer(), killOrKilledByWorld(phrase, v));
        }

        return kills;
    }

    private Integer killOrKilledByWorld(Phrase phrase, Integer value) {
        if(phrase.getAction().equals("killed") && phrase.getWho().equals(WORLD)) {
            return value - 1;
        } else if (phrase.getAction().equals("kill") && !phrase.getWho().equals(phrase.getPlayer())) {
            return value + 1;
        }

        return value;
    }
}
