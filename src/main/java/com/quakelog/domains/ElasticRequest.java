package com.quakelog.domains;

import lombok.Data;

@Data
public class ElasticRequest {
    private Long from;
    private Long size;
}
