package com.quakelog.domains;

import com.quakelog.domains.elastic.Hits;
import lombok.Data;

@Data
public class ElasticResponse {
    private Hits hits;
    private float took;
}
