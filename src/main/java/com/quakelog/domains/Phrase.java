package com.quakelog.domains;

import lombok.Data;

@Data
public class Phrase {
    private String player;
    private String action;
    private String who;
    private String cause;
}
