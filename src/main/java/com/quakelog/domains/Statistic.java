package com.quakelog.domains;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class Statistic {
    private String name;
    private Integer totalKills;
    private Map<String, Integer> kills;
}
