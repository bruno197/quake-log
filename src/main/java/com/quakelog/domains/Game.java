package com.quakelog.domains;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Game implements Cloneable{
    private String match;
    private List<Phrase> phraseList;
}
