package com.quakelog.domains.elastic;

import lombok.Data;

@Data
public class SubHits {
    private Source _source;
}
