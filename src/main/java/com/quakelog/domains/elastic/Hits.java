package com.quakelog.domains.elastic;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Hits {
    private float total;
    private float max_score;
    private ArrayList<SubHits> hits = new ArrayList <SubHits> ();
}
