package com.quakelog.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "app", ignoreUnknownFields = false)
public class AppProperties {
    @Getter
    @Setter
    private QuakeLog quakeLog = new QuakeLog();

    @Getter
    @Setter
    public class QuakeLog {
        private String fileName = "";

        private String filePath = "";
    }
}
