package com.quakelog.gateways.filesystem;

import com.quakelog.gateways.ParserApiGateway;
import com.quakelog.gateways.exception.ParserFileSystemGatewayException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ParserFileSystemGatewayImpl implements ParserApiGateway {
    private static final String UTF_8 = "UTF-8";

    @Value("${app.quakeLog.fileName}")
    private String fileName;

    @Value("${app.quakeLog.filePath}")
    private String filePath;

    @Override
    public List<String> getLines() {
        List<String> lines = new ArrayList<>();
        LineIterator it = null;
        try {
            it = FileUtils.lineIterator(new File(
                    filePath +
                            fileName), UTF_8);
            while (it.hasNext()) {
                lines.add(it.nextLine());
            }
        } catch (IOException e) {
            log.error("Error to read a file ", e.getStackTrace());
            throw new ParserFileSystemGatewayException();
        } finally {
            LineIterator.closeQuietly(it);
            return lines;
        }
    }
}
