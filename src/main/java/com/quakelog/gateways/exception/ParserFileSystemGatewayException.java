package com.quakelog.gateways.exception;

public class ParserFileSystemGatewayException extends RuntimeException {
    private static final long serialVersionUID = 5502347862043183093L;

    private static final String MESSAGE = "Erro ao trabalhar com o arquivo";

    public ParserFileSystemGatewayException() {
        super(MESSAGE);
    }
}
