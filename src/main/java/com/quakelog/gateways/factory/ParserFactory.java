package com.quakelog.gateways.factory;

import com.quakelog.gateways.ParserApiGateway;
import com.quakelog.gateways.feign.ParserElasticGatewayImpl;
import com.quakelog.gateways.filesystem.ParserFileSystemGatewayImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParserFactory {
    @Autowired
    private ParserElasticGatewayImpl parserElasticGateway;

    @Autowired
    private ParserFileSystemGatewayImpl parserFileSystemGateway;

    public ParserApiGateway create(boolean onElastic){

        if(onElastic){
            return parserElasticGateway;
        } else {
            return parserFileSystemGateway;
        }
    }
}
