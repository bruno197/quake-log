package com.quakelog.gateways;

import java.util.List;

@FunctionalInterface
public interface ParserApiGateway {
    List<String> getLines();
}
