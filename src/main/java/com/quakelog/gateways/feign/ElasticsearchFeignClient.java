package com.quakelog.gateways.feign;

import com.quakelog.domains.ElasticRequest;
import com.quakelog.domains.ElasticResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@FeignClient(name = "elasticsearch", url="${elasticsearch.host}")
@Component
public interface ElasticsearchFeignClient {
    @RequestMapping(method = POST,
            value = "/quake/_search",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    ElasticResponse request(@RequestBody final ElasticRequest jsonRequest);
}