    package com.quakelog.gateways.feign;

    import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
    import com.quakelog.domains.ElasticRequest;
    import com.quakelog.domains.ElasticResponse;
    import com.quakelog.domains.elastic.SubHits;
    import com.quakelog.gateways.ParserApiGateway;
    import com.quakelog.gateways.filesystem.ParserFileSystemGatewayImpl;
    import lombok.extern.slf4j.Slf4j;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.stereotype.Service;

    import java.util.ArrayList;
    import java.util.List;

    @Slf4j
    @Service
    public class ParserElasticGatewayImpl implements ParserApiGateway {
        @Autowired
        private ElasticsearchFeignClient elasticsearchFeignClient;

        @Autowired
        private ParserFileSystemGatewayImpl parserFileSystemGateway;

        @Override
        @HystrixCommand(fallbackMethod = "reliable")
        public List<String> getLines() {
            ElasticRequest request = new ElasticRequest();
            request.setFrom(0L);
            request.setSize(100L);
            final ElasticResponse response = elasticsearchFeignClient.request(request);

            List<String> lines = new ArrayList<>();

            for (SubHits subHits: response.getHits().getHits()) {
                subHits.get_source().getMessage();
                for(String line : subHits.get_source().getMessage().split("\n")) {
                    lines.add(line);
                }
            }
            return lines;
        }

        public List<String> reliable() {
            return parserFileSystemGateway.getLines();
        }
    }
