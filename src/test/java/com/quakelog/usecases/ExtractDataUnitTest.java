package com.quakelog.usecases;

import com.quakelog.domains.Game;
import com.quakelog.gateways.factory.ParserFactory;
import com.quakelog.gateways.filesystem.ParserFileSystemGatewayImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ExtractDataUnitTest {
    @Mock
    private ParserGameData parserGameData;

    @Mock
    private ParserFactory parserFactory;

    @Mock
    private ParserFileSystemGatewayImpl parserFileSystemGateway;

    @InjectMocks
    private ExtractData extractData;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldReturnGames() {
        System.out.println("Present Project Directory : "+ System.getProperty("user.dir"));

        ReflectionTestUtils.setField(parserFileSystemGateway, "filePath", System.getProperty("user.dir")+"/extras/");
        ReflectionTestUtils.setField(parserFileSystemGateway, "fileName", "games.log");
        ReflectionTestUtils.setField(extractData, "elasticsearch", false);


        when(parserFactory.create(false)).thenReturn(parserFileSystemGateway);
        when(parserFileSystemGateway.getLines()).thenReturn(loadAnyLogWithoutGame());
        when(parserGameData.getGame(any(ArrayList.class))).thenReturn(new ArrayList<>());
        List<Game> games = extractData.readData();

        assertNotNull(games);
    }

    private List<String> loadAnyLogWithoutGame() {
        return Arrays.asList(
                " 20:37 ------------------------------------------------------------        ",
                " 20:37 InitGame:                                                           ",
                " 20:38 ClientConnect: 2                                                    ",
                " 20:38 ClientUserinfoChanged: 2                                            ",
                " 20:38 ClientBegin: 2                                                      ",
                " 20:40 Item: 2 weapon_rocketlauncher                                       ",
                " 20:40 Item: 2 ammo_rockets                                                ",
                " 20:42 Item: 2 item_armor_body                                             ",
                " 14:11 ------------------------------------------------------------"
        );
    }

}