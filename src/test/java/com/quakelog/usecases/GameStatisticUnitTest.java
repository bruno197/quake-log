package com.quakelog.usecases;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.quakelog.domains.DomainTemplateLoader;
import com.quakelog.domains.Game;
import com.quakelog.domains.Statistic;
import com.quakelog.domains.templates.GameTemplate;
import org.apache.commons.lang3.ClassUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class GameStatisticUnitTest {
    @Mock
    private ExtractData extractData;

    @InjectMocks
    private GameStatistic gameStatistic;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(DomainTemplateLoader.class));
    }

    @Test
    public void shouldReturnStatisticList() {
        List<Game> games = new ArrayList<>();
        games.add(Fixture.from(Game.class).gimme(GameTemplate.ANY_GAME));

        when(extractData.readData()).thenReturn(games);
        List<Statistic> statisticList = gameStatistic.checkstats();

        assertNotNull(statisticList);
        assertFalse(statisticList.isEmpty());
        assertEquals("game_1", statisticList.get(0).getName());
    }
}