package com.quakelog.usecases;

import com.quakelog.domains.Game;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ParserGameDataUnitTest {
    @InjectMocks
    private ParserGameData parserGameData;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldReturnGameWithKills() {
        List<Game> games = parserGameData.getGame(loadAnyLog());

        assertNotNull(games);
        assertEquals(1, games.size());
        assertEquals(11, games.get(0).getPhraseList().size());
        assertEquals("game_1", games.get(0).getMatch());
    }

    @Test
    public void shouldReturnMoreThanOneGame() {
        List<String> all = new ArrayList<>();
        all.addAll(loadAnyLog());
        all.addAll(loadAnyLog());
        List<Game> games = parserGameData.getGame(all);

        assertNotNull(games);
        assertEquals(2, games.size());
        assertEquals(11, games.get(0).getPhraseList().size());
        assertEquals("game_1", games.get(0).getMatch());
        assertEquals("game_2", games.get(1).getMatch());

    }

    @Test
    public void shouldReturnEmptyGame() {
        List<Game> games = parserGameData.getGame(loadAnyLogWithoutGame());

        assertNotNull(games);
        assertEquals(0, games.size());
    }

    private List<String> loadAnyLog(){
        return Arrays.asList(
                " 20:37 ------------------------------------------------------------        ",
                " 20:37 InitGame:                                                           ",
                " 20:38 ClientConnect: 2                                                    ",
                " 20:38 ClientUserinfoChanged: 2                                            ",
                " 20:38 ClientBegin: 2                                                      ",
                " 20:40 Item: 2 weapon_rocketlauncher                                       ",
                " 20:40 Item: 2 ammo_rockets                                                ",
                " 20:42 Item: 2 item_armor_body                                             ",
                " 20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT      ",
                " 20:59 Item: 2 weapon_rocketlauncher                                       ",
                " 21:04 Item: 2 ammo_shells                                                 ",
                " 21:07 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT      ",
                " 21:10 ClientDisconnect: 2                                                 ",
                " 21:15 ClientConnect: 2                                                    ",
                " 21:15 ClientUserinfoChanged: 2                                          ",
                " 21:17 ClientUserinfoChanged: 2                                          ",
                " 21:17 ClientBegin: 2                                                      ",
                " 21:18 Item: 2 weapon_rocketlauncher                                       ",
                " 21:21 Item: 2 item_armor_body                                             ",
                " 21:32 Item: 2 item_health_large                                           ",
                " 21:33 Item: 2 weapon_rocketlauncher                                       ",
                " 21:34 Item: 2 ammo_rockets                                                ",
                " 21:42 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT      ",
                " 21:49 Item: 2 weapon_rocketlauncher                                       ",
                " 21:51 ClientConnect: 3                                                    ",
                " 21:51 ClientUserinfoChanged: 3                               ",
                " 21:53 ClientUserinfoChanged: 3                                  ",
                " 21:53 ClientBegin: 3                                                      ",
                " 22:04 Item: 2 weapon_rocketlauncher                                       ",
                " 22:04 Item: 2 ammo_rockets                                                ",
                " 22:06 Kill: 2 3 7: Isgalamido killed Mocinha by MOD_ROCKET_SPLASH         ",
                " 22:11 Item: 2 item_quad                                                   ",
                " 22:11 ClientDisconnect: 3                                                 ",
                " 22:18 Kill: 2 2 7: Isgalamido killed Isgalamido by MOD_ROCKET_SPLASH      ",
                " 22:26 Item: 2 weapon_rocketlauncher                                       ",
                " 22:27 Item: 2 ammo_rockets                                                ",
                " 22:40 Kill: 2 2 7: Isgalamido killed Isgalamido by MOD_ROCKET_SPLASH      ",
                " 22:43 Item: 2 weapon_rocketlauncher                                       ",
                " 22:45 Item: 2 item_armor_body                                             ",
                " 23:06 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT      ",
                " 23:09 Item: 2 weapon_rocketlauncher                                       ",
                " 23:10 Item: 2 ammo_rockets                                                ",
                " 23:25 Item: 2 item_health_large                                           ",
                " 23:30 Item: 2 item_health_large                                           ",
                " 23:32 Item: 2 weapon_rocketlauncher                                       ",
                " 23:35 Item: 2 item_armor_body                                             ",
                " 23:36 Item: 2 ammo_rockets                                                ",
                " 23:37 Item: 2 weapon_rocketlauncher                                       ",
                " 23:40 Item: 2 item_armor_shard                                            ",
                " 23:40 Item: 2 item_armor_shard                                            ",
                " 23:40 Item: 2 item_armor_shard                                            ",
                " 23:40 Item: 2 item_armor_combat                                           ",
                " 23:43 Item: 2 weapon_rocketlauncher                                       ",
                " 23:57 Item: 2 weapon_shotgun                                              ",
                " 23:58 Item: 2 ammo_shells                                                 ",
                " 24:13 Item: 2 item_armor_shard                                            ",
                " 24:13 Item: 2 item_armor_shard                                            ",
                " 24:13 Item: 2 item_armor_shard                                            ",
                " 24:13 Item: 2 item_armor_combat                                           ",
                " 24:16 Item: 2 item_health_large                                           ",
                " 24:18 Item: 2 ammo_rockets                                                ",
                " 24:19 Item: 2 weapon_rocketlauncher                                       ",
                " 24:22 Item: 2 item_armor_body                                             ",
                " 24:24 Item: 2 ammo_rockets                                                ",
                " 24:24 Item: 2 weapon_rocketlauncher                                       ",
                " 24:36 Item: 2 item_health_large                                           ",
                " 24:43 Item: 2 item_health_mega                                            ",
                " 25:05 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT      ",
                " 25:09 Item: 2 weapon_rocketlauncher                                       ",
                " 25:09 Item: 2 ammo_rockets                                                ",
                " 25:11 Item: 2 item_armor_body                                             ",
                " 25:18 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT      ",
                " 25:21 Item: 2 weapon_rocketlauncher                                       ",
                " 25:22 Item: 2 ammo_rockets                                                ",
                " 25:34 Item: 2 weapon_rocketlauncher                                       ",
                " 25:41 Kill: 1022 2 19: <world> killed Isgalamido by MOD_FALLING           ",
                " 25:50 Item: 2 item_armor_combat                                           ",
                " 25:52 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT      ",
                " 25:54 Item: 2 ammo_rockets                                                ",
                " 25:55 Item: 2 weapon_rocketlauncher                                       ",
                " 25:55 Item: 2 weapon_rocketlauncher                                       ",
                " 25:59 Item: 2 item_armor_shard                                            ",
                " 25:59 Item: 2 item_armor_shard                                            ",
                " 26:05 Item: 2 item_armor_shard                                            ",
                " 26:05 Item: 2 item_armor_shard                                            ",
                " 26:05 Item: 2 item_armor_shard                                            ",
                " 26:09 Item: 2 weapon_rocketlauncher                                       ",
                " 14:11 ------------------------------------------------------------"
        );
    }

    private List<String> loadAnyLogWithoutGame() {
        return Arrays.asList(
                " 20:37 ------------------------------------------------------------        ",
                " 20:37 InitGame:                                                           ",
                " 20:38 ClientConnect: 2                                                    ",
                " 20:38 ClientUserinfoChanged: 2                                            ",
                " 20:38 ClientBegin: 2                                                      ",
                " 20:40 Item: 2 weapon_rocketlauncher                                       ",
                " 20:40 Item: 2 ammo_rockets                                                ",
                " 20:42 Item: 2 item_armor_body                                             ",
                " 14:11 ------------------------------------------------------------"
        );
    }

}