package com.quakelog.gateways.factory;

import com.quakelog.gateways.ParserApiGateway;
import com.quakelog.gateways.feign.ParserElasticGatewayImpl;
import com.quakelog.gateways.filesystem.ParserFileSystemGatewayImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;

public class ParserFactoryUnitTest {
    @InjectMocks
    private ParserFactory parserFactory;

    @Mock
    private ParserFileSystemGatewayImpl parserFileSystemGateway;

    @Mock
    private ParserElasticGatewayImpl parserElasticGateway;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldReturnFileSystem() {
        ParserApiGateway parserApiGateway = parserFactory.create(false);

        assertThat(parserApiGateway, instanceOf(ParserFileSystemGatewayImpl.class));
    }

    @Test
    public void shouldReturnElastic() {
        ParserApiGateway parserApiGateway = parserFactory.create(true);

        assertThat(parserApiGateway, instanceOf(ParserElasticGatewayImpl.class));
    }
}