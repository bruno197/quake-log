package com.quakelog.gateways.filesystem;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ParserFileSystemGatewayImpl.class})
public class ParserFileSystemGatewayImplUnitTest {
    @Autowired
    private ParserFileSystemGatewayImpl parserFileSystemGateway;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldWork() {
        ReflectionTestUtils.setField(parserFileSystemGateway, "filePath", System.getProperty("user.dir")+"/extras/");
        ReflectionTestUtils.setField(parserFileSystemGateway, "fileName", "games.log");

        List<String> logLines = parserFileSystemGateway.getLines();

        assertNotNull(logLines);
        assertFalse(logLines.isEmpty());
    }
}