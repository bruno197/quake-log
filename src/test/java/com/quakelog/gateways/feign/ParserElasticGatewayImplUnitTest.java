package com.quakelog.gateways.feign;

import com.quakelog.domains.ElasticRequest;
import com.quakelog.domains.ElasticResponse;
import com.quakelog.domains.elastic.Hits;
import com.quakelog.domains.elastic.Source;
import com.quakelog.domains.elastic.SubHits;
import com.quakelog.gateways.filesystem.ParserFileSystemGatewayImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        ParserElasticGatewayImpl.class,
        ElasticsearchFeignClient.class,
        ParserFileSystemGatewayImpl.class
})
public class ParserElasticGatewayImplUnitTest {
    @Autowired
    private ParserElasticGatewayImpl parserElasticGateway;

    @MockBean
    ElasticsearchFeignClient elasticsearchFeignClient;


    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldWork() {
        ElasticResponse elasticResponse = new ElasticResponse();
        Hits hits = new Hits();
        SubHits subHits = new SubHits();
        subHits.set_source(new Source(getMessage()));
        hits.setTotal(10L);
        hits.getHits().add(subHits);
        elasticResponse.setTook(10L);
        elasticResponse.setHits(hits);

        doReturn(elasticResponse) //
                .when(elasticsearchFeignClient).request(any(ElasticRequest.class));
        List<String> logLines = parserElasticGateway.getLines();

        assertNotNull(logLines);
        assertFalse(logLines.isEmpty());
    }

    private String getMessage() {
        return "  0:00 InitGame: \\\\sv_floodProtect\\\\1\\\\sv_maxPing\\\\0\\\\sv_minPing\\\\0\\\\sv_maxRate\\\\10000\\\\sv_minRate\\\\0\\\\sv_hostname\\\\Code Miner Server\\\\g_gametype\\\\0\\\\sv_privateClients\\\\2\\\\sv_maxclients\\\\16\\\\sv_allowDownload\\\\0\\\\dmflags\\\\0\\\\fraglimit\\\\20\\\\timelimit\\\\15\\\\g_maxGameClients\\\\0\\\\capturelimit\\\\8\\\\version\\\\ioq3 1.36 linux-x86_64 Apr 12 2009\\\\protocol\\\\68\\\\mapname\\\\q3dm17\\\\gamename\\\\baseq3\\\\g_needpass\\\\0\\n  0:25 ClientConnect: 2\\n  0:25 ClientUserinfoChanged: 2 n\\\\Dono da Bola\\\\t\\\\0\\\\model\\\\sarge/krusade\\\\hmodel\\\\sarge/krusade\\\\g_redteam\\\\\\\\g_blueteam\\\\\\\\c1\\\\5\\\\c2\\\\5\\\\hc\\\\95\\\\w\\\\0\\\\l\\\\0\\\\tt\\\\0\\\\tl\\\\0\\n  0:27 ClientUserinfoChanged: 2 n\\\\Mocinha\\\\t\\\\0\\\\model\\\\sarge\\\\hmodel\\\\sarge\\\\g_redteam\\\\\\\\g_blueteam\\\\\\\\c1\\\\4\\\\c2\\\\5\\\\hc\\\\95\\\\w\\\\0\\\\l\\\\0\\\\tt\\\\0\\\\tl\\\\0\\n  0:27 ClientBegin: 2\\n  0:29 Item: 2 weapon_rocketlauncher\\n  0:35 Item: 2 item_armor_shard\\n  0:35 Item: 2 item_armor_shard\\n  0:35 Item: 2 item_armor_shard\\n  0:35 Item: 2 item_armor_combat\\n  0:38 Item: 2 item_armor_shard\\n  0:38 Item: 2 item_armor_shard\\n  0:38 Item: 2 item_armor_shard\\n  0:55 Item: 2 item_health_large\\n  0:56 Item: 2 weapon_rocketlauncher\\n  0:57 Item: 2 ammo_rockets\\n  0:59 ClientConnect: 3\\n  0:59 ClientUserinfoChanged: 3 n\\\\Isgalamido\\\\t\\\\0\\\\model\\\\xian/default\\\\hmodel\\\\xian/default\\\\g_redteam\\\\\\\\g_blueteam\\\\\\\\c1\\\\4\\\\c2\\\\5\\\\hc\\\\100\\\\w\\\\0\\\\l\\\\0\\\\tt\\\\0\\\\tl\\\\0\\n  1:01 ClientUserinfoChanged: 3 n\\\\Isgalamido\\\\t\\\\0\\\\model\\\\uriel/zael\\\\hmodel\\\\uriel/zael\\\\g_redteam\\\\\\\\g_blueteam\\\\\\\\c1\\\\5\\\\c2\\\\5\\\\hc\\\\100\\\\w\\\\0\\\\l\\\\0\\\\tt\\\\0\\\\tl\\\\0\\n  1:01 ClientBegin: 3\\n  1:02 Item: 3 weapon_rocketlauncher\\n  1:04 Item: 2 item_armor_shard\\n  1:04 Item: 2 item_armor_shard\\n  1:04 Item: 2 item_armor_shard\\n  1:06 ClientConnect: 4\\n  1:06 ClientUserinfoChanged: 4 n\\\\Zeh\\\\t\\\\0\\\\model\\\\sarge/default\\\\hmodel\\\\sarge/default\\\\g_redteam\\\\\\\\g_blueteam\\\\\\\\c1\\\\5\\\\c2\\\\5\\\\hc\\\\100\\\\w\\\\0\\\\l\\\\0\\\\tt\\\\0\\\\tl\\\\0\\n  1:08 Kill: 3 2 6: Isgalamido killed Mocinha by MOD_ROCKET\\n  1:08 ClientUserinfoChanged: 4 n\\\\Zeh\\\\t\\\\0\\\\model\\\\sarge/default\\\\hmodel\\\\sarge/default\\\\g_redteam\\\\\\\\g_blueteam\\\\\\\\c1\\\\1\\\\c2\\\\5\\\\hc\\\\100\\\\w\\\\0\\\\l\\\\0\\\\tt\\\\0\\\\tl\\\\0\\n  1:08 ClientBegin: 4\\n  1:10 Item: 3 item_armor_shard\\n  1:10 Item: 3 item_armor_shard\\n  1:10 Item: 3 item_armor_shard\\n  1:10 Item: 3 item_armor_combat\\n  1:11 Item: 4 weapon_shotgun\\n  1:11 Item: 4 ammo_shells\\n  1:16 Item: 4 item_health_large\\n  1:18 Item: 4 weapon_rocketlauncher\\n  1:18 Item: 4 ammo_rockets\\n  1:26 Kill: 1022 4 22: <world> killed Zeh by MOD_TRIGGER_HURT\\n  1:26 ClientUserinfoChanged: 2 n\\\\Dono da Bola\\\\t\\\\0\\\\model\\\\sarge\\\\hmodel\\\\sarge\\\\g_redteam\\\\\\\\g_blueteam\\\\\\\\c1\\\\4\\\\c2\\\\5\\\\hc\\\\95\\\\w\\\\0\\\\l\\\\0\\\\tt\\\\0\\\\tl\\\\0\\n  1:26 Item: 3 weapon_railgun\\n  1:29 Item: 2 weapon_rocketlauncher\\n  1:29 Item: 3 weapon_railgun\\n  1:32 Item: 3 weapon_railgun\\n  1:32 Kill: 1022 4 22: <world> killed Zeh by MOD_TRIGGER_HURT\\n  1:35 Item: 2 item_armor_shard\\n  1:35 Item: 2 item_armor_shard\\n  1:35 Item: 2 item_armor_shard\\n  1:35 Item: 3 weapon_railgun\\n  1:38 Item: 2 item_health_large\\n  1:38 Item: 3 weapon_railgun\\n  1:41 Kill: 1022 2 19: <world> killed Dono da Bola by MOD_FALLING\\n  1:41 Item: 3 weapon_railgun\\n  1:43 Item: 2 ammo_rockets\\n  1:44 Item: 2 weapon_rocketlauncher\\n  1:46 Item: 2 item_armor_shard\\n  1:47 Item: 2 item_armor_shard\\n  1:47 Item: 2 item_armor_shard\\n  1:47 ShutdownGame:\\n  1:47 ------------------------------------------------------------";
    }
}