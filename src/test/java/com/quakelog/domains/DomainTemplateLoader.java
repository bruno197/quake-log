package com.quakelog.domains;

import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.quakelog.domains.templates.GameTemplate;
import com.quakelog.domains.templates.PhraseTemplate;
import com.quakelog.domains.templates.StatisticTemplate;

public class DomainTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        GameTemplate.loadTemplates();
        PhraseTemplate.loadTemplates();
        StatisticTemplate.loadTemplates();
    }
}
