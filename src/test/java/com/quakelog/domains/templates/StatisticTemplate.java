package com.quakelog.domains.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import com.quakelog.domains.Statistic;

import java.util.HashMap;
import java.util.Map;

public class StatisticTemplate {
    public static void loadTemplates() {
        anyStatistic();
    }

    public static final String ANY_STATISTIC = "ANY_STATISTIC";
    private static void anyStatistic() {
        Fixture.of(Statistic.class).addTemplate(ANY_STATISTIC,
                new Rule() {{
                    add("name", "game_1");
                    add("totalKills", 54);
                    add("kills", getKills());
                }});
    }

    private static Map<String, Integer> getKills() {
        Map<String, Integer> kills = new HashMap<>();
        kills.put("John", 10);
        kills.put("Red", 15);
        kills.put("Rob", 10);

        return kills;
    }
}
