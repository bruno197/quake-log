package com.quakelog.domains.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import com.quakelog.domains.Game;
import com.quakelog.domains.Phrase;

public class GameTemplate {

    public static void loadTemplates() {
        anyGame();
    }

    public static final String ANY_GAME = "ANY_GAME";
    private static void anyGame() {
        Fixture.of(Game.class).addTemplate(ANY_GAME,
                new Rule() {{
                    add("match", "game_1");
                    add("phraseList", has(3).of(Phrase.class, PhraseTemplate.ANY_PHRASE, PhraseTemplate.ANY_SUICIDE, PhraseTemplate.ANY_WORLD));
                }});
    }
}
