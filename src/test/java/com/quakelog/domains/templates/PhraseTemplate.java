package com.quakelog.domains.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import com.quakelog.domains.Phrase;

public class PhraseTemplate {
    public static void loadTemplates() {
        anyPhrase();
        anyWorld();
        anySuicide();
    }

    public static final String ANY_PHRASE = "ANY_PHRASE";
    private static void anyPhrase() {
        Fixture.of(Phrase.class).addTemplate(ANY_PHRASE,
                new Rule() {{
                    add("player", "John");
                    add("action", "kill");
                    add("who", "Tod");
                    add("cause", "MOD_SHUTGUN");
                }});
    }

    public static final String ANY_WORLD = "ANY_WORLD";
    private static void anyWorld() {
        Fixture.of(Phrase.class).addTemplate(ANY_WORLD,
                new Rule() {{
                    add("player", "John");
                    add("action", "killed");
                    add("who", "<world>");
                    add("cause", "MOD_SHUTGUN");
                }});
    }

    public static final String ANY_SUICIDE = "ANY_SUICIDE";
    private static void anySuicide() {
        Fixture.of(Phrase.class).addTemplate(ANY_SUICIDE,
                new Rule() {{
                    add("player", "John");
                    add("action", "kill");
                    add("who", "John");
                    add("cause", "MOD_SHUTGUN");
                }});
    }
}
