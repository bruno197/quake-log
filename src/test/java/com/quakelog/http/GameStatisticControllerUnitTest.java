package com.quakelog.http;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.quakelog.domains.DomainTemplateLoader;
import com.quakelog.domains.Statistic;
import com.quakelog.domains.templates.StatisticTemplate;
import com.quakelog.usecases.GameStatistic;
import org.apache.commons.lang3.ClassUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
@ContextConfiguration(classes = { GameStatisticController.class})
public class GameStatisticControllerUnitTest {
    private static final String URL_TEMPLATE = "/api/stats";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GameStatistic gameStatistic;

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(DomainTemplateLoader.class));
    }

    @Test
    public void shouldGetNewHumanSuccessfully() throws Exception {
        // Given
        List<Statistic> list = new ArrayList<>();
        list.add(Fixture.from(Statistic.class).gimme(StatisticTemplate.ANY_STATISTIC));
        Mockito.when(gameStatistic.checkstats()).thenReturn(list);

        // When
        ResultActions resultActions = mockMvcPerform();

        // Then
        resultActions.andExpect(status().isOk());
    }

    private ResultActions mockMvcPerform() throws Exception {
        final MockHttpServletRequestBuilder builder = get(URL_TEMPLATE) //
                .accept(MediaType.APPLICATION_JSON) //
                .contentType(MediaType.APPLICATION_JSON);

        return mockMvc.perform(builder);
    }
}