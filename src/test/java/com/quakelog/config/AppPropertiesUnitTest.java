package com.quakelog.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { AppPropertiesUnitTest.TestConfiguration.class })
@TestPropertySource(properties = {
        "app.quakeLog.fileName=games.log",
        "app.quakeLog.filePath=/opt/log",
})
public class AppPropertiesUnitTest {
    public static final String GAMES_LOG = "games.log";
    public static final String OPT_LOG = "/opt/log";
    @Autowired
    private AppProperties properties;

    @Test
    public void should_Populate_AppProperties() {
        assertEquals(GAMES_LOG, properties.getQuakeLog().getFileName());
        assertEquals(OPT_LOG, properties.getQuakeLog().getFilePath());
    }

    @EnableConfigurationProperties(AppProperties.class)
    public static class TestConfiguration {
        // nothing
    }
}