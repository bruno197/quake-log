FROM openjdk:8-jdk-alpine

LABEL SERVICE_NAME=quake_log
ENV APP_NAME quake-log.jar
ENV USER_NAME service
ENV APP_HOME /opt/quake_log/$USER_NAME

ENV LANG pt_BR.UTF-8

RUN mkdir -p $APP_HOME

ADD target/*.jar ${APP_HOME}/${APP_NAME}
ADD extras/games.log $APP_HOME

RUN sh -c 'touch $APP_HOME/$APP_NAME'

WORKDIR $APP_HOME

ENTRYPOINT [ "sh", "-c", "java -jar $APP_NAME" ]
ENV TZ 'America/Sao_Paulo'