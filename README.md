# Quake log parser


## Requisitos de sistema

Para gerar e rodar a aplicação você vai precisar ter instalado:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) (local)
- [Maven 3](https://maven.apache.org) (local)
- [Docker](https://docs.docker.com/)

## Rodando a aplicação local

**IMPORTANTE:**
Antes de executar a aplicação, altere o caminho do arquivo de log no [application.yml](./src/main/resources/application.yml)
```app:
   quakeLog:
     fileName: nomeArquivoLog.log
     filePath: /camiho/fisico/do/arquivo/log/
```
Existem várias maneiras de executar um aplicativo Spring Boot em sua máquina local. Uma maneira é executar o método main na classe com.quakelog.QuakeLogApplication na sua IDE.

Alternativamente, você pode usar o [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) com o comando:

```shell
mvn spring-boot:run
```

Outra forma é gerar a imagem docker e rodar o container local. Para isso é necessário instalar o docker e seguir os comandos:

```shell
make build
```

Ao receber a mensagem de sucesso execute. A aplicação ficara expota na porta 8080.

```shell
make run
```

## Rodando a aplicação nos container

Toda os recursos da aplicação pode ser executado todando os seguintes comandos no docker-compose.yml;

```
docker-compose build && docker-compose up
```

Para desligar
```
docker-compose down
``` 

### Testando o endpoint
```
curl -v http://localhost:8080/api/stats
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /api/stats HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> 
< HTTP/1.1 200 
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Sun, 30 Sep 2018 21:06:40 GMT
< 
* Connection #0 to host localhost left intact
```

Detalhes da API no endereço do swagger

[swagger-ui](http://localhost:8080/swagger-ui.html)

### Testando fallback
Para testar o fallback da aplicação, inicie a aplicação sem o container do Elasticsearsh.

### Rodando os testes da aplicação
Execute o comando:
```
mvn clean test
```

Testes unitários na camada de negócio e conecxão com banco de dados.
90% de cobertura desconsiderando classes de domain, json e exception.

![coverage](./extras/coverage.png)

### Solução da aplicação

Uma instancia do Filebeat escuta a pasta de log do servidor, com uma rotina para ler o arquivo e indexar os registros no Elasticsearch.
Ao efetuar a chamada GET na API, a aplicação consulta no elasticsearsh os registros salvos, processa e restorna.

Caso ocorra alguma indisponibilidade por parte do elsaticsearsh, a aplicação possui uma regra de fallback para contingencia, lendo o arquivo do filesystem e fazendo os mesmos processamentos.

**OBSERVAÇÃO:** Como o filebeat não lê o arquivo linha por linha e o log não tem data (dd/mm/yyyy), ele não consegue agrupar por ordem cronológica, diferente quando é feito a leitura por arquivo, que assume a primeira linha como o primeiro registro, e assim sucessivamente.

Em resumo, quando o processo ocorrer pelo elasticsearch, o **response** vem em ordem diferente quando processo pelo arquivo (fallback, container do elasticsearch desligado).

### Detalhe técnico

Filebeat:
    Servidor usado apensa para escutar a pasta configurada no arquivo filebeat.yml. O pattern para leitura do arquivo é feita por uma regex e executando o flush com a mesma regex, fazendo recursividade nos blocos.
    
Elasticsearch:
    Utilizado apenas para armazenar os registros vindo do Filebeat. A consulta é feita pela API do Elastic utilizando o feingClient para possibilitar a implementação da regra de fallback.
    
Quake-log:
    App spring boot, com Java 8 seguindo os padrões do clean architecture.
    Controller documentado usando o swagger, retornando status 200 no caso de sucesso. 
    Camada de usecase para, primeiro, consultar registros e retornar uma lista de String (estratégia para deixar o processamento do registro genérico, em caso de novas implementações do gateway).
    Gateway com duas implementações, uma para ler de um arquivo, armazenando as linhas em uma lista. Outra com uma chamada feing e o split do bloco da string para gerar a mesma lista.